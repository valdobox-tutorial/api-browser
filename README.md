#  Swagger documentation viewer
The service contains the list of all documented services using the Swagger/Open-API specification(https://swagger.io/docs/).  

## Runtime
| Name           | Description                                                                            |
| :------------- | :------------------------------------------------------------------------------------- |
| Language       | ES6 / Node.js                                                                          |
| Main Framework | [fastify](https://www.fastify.io/), [Swagger UI](https://swagger.io/tools/swagger-ui/) |
| Depends on     | api-gateway, KeyCloak                                                                  |

# How is works?
Initially, when the Swagger UI web page is opened a KeyCloak login session is triggered. The user JWT is then used to fetch the services list and correspondent swagger docs from the services behind the gateway.

## Retrieving the services list from the gateway:
`GET ${API_GATEWAY_URL}/services.json`

## Interacting with the endpoints
Through this Swagger UI, developers can really interact with backend services using their credentials, in the same way they would do it from a web interface. 

## Running the service
```bash
# install dependencies
npm i

# run locally
npm start
```

## Dependencies to other services
- `Api-Gateway`: Provides access to the services list and also to the services documentation.
- `KeyCloak`: Identity and Access Management system. The place where authentication happens.
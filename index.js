const server = require('fastify')({ logger: false })
const path = require('path')

server.register(require('fastify-static'), {
  root: path.join(__dirname, 'swagger-ui')
})

server.get('/', (req, res) => {
  res.sendFile('index.html')
})

server.get('/health', (req, res) => {
  res.send({
    status: 'OK'
  })
})

server.listen(process.env.PORT || 3000, '0.0.0.0')